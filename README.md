# up to date dnsmasq ubuntu packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/dnsmasq.asc https://packaging.gitlab.io/dnsmasq/gpg.key
```

### Add repo to apt

```bash
. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/dnsmasq/${DISTRIB_CODENAME} ${DISTRIB_CODENAME}-dnsmasq-backport main" | sudo tee /etc/apt/sources.list.d/dnsmasq.list
```
