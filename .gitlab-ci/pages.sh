#!/bin/bash

set -e

dists=("bionic" "focal" "jammy")

for dist in "${dists[@]}"
do
    mkdir -p "${CI_PROJECT_DIR}"/public/"${dist}"
    cp -rv \
      "${CI_PROJECT_DIR}"/.repo/deb/"${dist}"/"${dist}"-dnsmasq-backport/{pool,dists} \
      "${CI_PROJECT_DIR}"/public/"${dist}"
    cp -rv \
      "${CI_PROJECT_DIR}"/"${dist}"-*/.version \
      "${CI_PROJECT_DIR}"/public/"${dist}"
done
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/deb/gpg.key \
  "${CI_PROJECT_DIR}"/public/
