#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

dists=("bionic" "focal" "jammy")

for dist in "${dists[@]}"
do
    for dir in "${CI_PROJECT_DIR}"/"${dist}"-*
    do
        export SCAN_DIR="${dir}"
        export CODENAME="${dist}-dnsmasq-backport"
        /deb.sh "${dist}"
    done
done
