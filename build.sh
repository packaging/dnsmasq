#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
export DEBIAN_FRONTEND=noninteractive
arch="$(dpkg --print-architecture)"

# shellcheck disable=SC1091
. /etc/lsb-release

printf "\e[1;36mInstalling toolchain #1 ...\e[0m\n"
apt-get -qq update
apt-get -qqy install \
    curl \
    debian-keyring \
    jq

printf "\e[1;36mFetching upstream version ...\e[0m"
upstream="$(curl -s "https://sources.debian.org/api/src/dnsmasq/?suite=${UPSTREAM_SUITE:-trixie}" | jq -re '.versions[].version')"
printf "\e[0;32m %s\e[0m\n" "${upstream}"
dnsmasq="${upstream%%-*}"

if [ "${FORCE}" == "true" ]; then
    current="0"
else
    printf "\e[1;36mFetching current repo version ...\e[0m"
    current=$(curl -sfkL "https://packaging.gitlab.io/dnsmasq/${DISTRIB_CODENAME}/.version" || echo "0")
fi
printf "\e[0;32m %s\e[0m\n" "${current}"

if dpkg --compare-versions "${upstream}" gt "${current}"; then
    printf "\e[1;36mBuilding new version ...\e[0m\n"
    printf "\e[1;36mInstalling toolchain #2 ...\e[0m\n"
    apt-get -qqy install \
        devscripts \
        equivs \
        ccache
    export PATH="/usr/lib/ccache/bin/:$PATH"
    export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR:-/tmp}/ccache}"
    mkdir -p "$CCACHE_DIR"
    tmpdir="$(mktemp -d)"
    cd "${tmpdir}"
    if [ -f "${script_dir}/.build/${DISTRIB_CODENAME}.sh" ]; then
        "${script_dir}/.build/${DISTRIB_CODENAME}.sh"
    fi
    for file in "dnsmasq_${upstream}.dsc" "dnsmasq_${dnsmasq}.orig.tar.xz" "dnsmasq_${dnsmasq}.orig.tar.xz.asc" "dnsmasq_${upstream}.debian.tar.xz"
    do
        curl -sLO "http://deb.debian.org/debian/pool/main/d/dnsmasq/${file}"
    done
    dpkg-source -x "dnsmasq_${upstream}.dsc"
    mk-build-deps "dnsmasq-${dnsmasq}/debian/control"
    apt-get -y install "./dnsmasq-build-deps_${upstream}_${arch}.deb"
    rm -f "./dnsmasq-build-deps_${upstream}_${arch}.deb"
    cd "dnsmasq-${dnsmasq}"
    debuild \
        --preserve-envvar=CCACHE_DIR \
        --prepend-path=/usr/lib/ccache \
        --no-lintian \
        -us -uc \
        -j"$(getconf _NPROCESSORS_ONLN)"
    uid="$(stat -c %u "${script_dir}"/build.sh)"
    gid="$(stat -c %g "${script_dir}"/build.sh)"
    install -d -o "${uid}" -g "${gid}" "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}"
    install -o "${uid}" -g "${gid}" -m 644 "${tmpdir}"/*.deb "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}/"
    echo "${upstream}" > "${tmpdir}"/.version
    install -o "${uid}" -g "${gid}" -m 644 "${tmpdir}"/.version "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}/"
fi
