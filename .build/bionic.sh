#!/bin/bash

set -e

apt-get -t bionic-backports -qqy install debhelper
build() {
    local package
    package="${1}"
    package_name="${package##*/}"
    if [ ! -f "${package##*/}" ]; then
        printf "\e[0;36mDownloading %s ...\e[0m\n" "${package_name}"
        curl -sLO "${package}"
        if [ "${package##*.}" == "dsc" ]; then
            printf "\e[0;36mBuilding %s ...\e[0m\n" "${package_name}"
            dpkg-source -x "${package##*/}" 2>&1 | tee "${package##*/}".dir
            dir="$(grep 'extracting\s.*\sin\s.*' "${package_name}".dir | awk '{print $NF}')"
            mk-build-deps "${dir}/debian/control" 2>&1 | tee "${package_name}".dep
            builddeps="$(grep -o '../.*build-deps.*.deb' "${package_name}".dep)"
            echo "builddeps = $builddeps"
            apt-get -y install "./${builddeps##*/}" && rm -f "./${builddeps##*/}"
            cd "${dir}" || exit 1
            debuild \
                --preserve-envvar=CCACHE_DIR \
                --prepend-path=/usr/lib/ccache \
                --no-lintian \
                -us -uc \
                -j"$(getconf _NPROCESSORS_ONLN)"
            cd ..
            apt-get -y install ./*.deb
        fi
    fi
}

packages=("http://archive.ubuntu.com/ubuntu/pool/main/libn/libnftnl/libnftnl_1.1.5-1.debian.tar.xz")
packages+=("http://archive.ubuntu.com/ubuntu/pool/main/libn/libnftnl/libnftnl_1.1.5.orig.tar.bz2.asc")
packages+=("http://archive.ubuntu.com/ubuntu/pool/main/libn/libnftnl/libnftnl_1.1.5.orig.tar.bz2")
packages+=("http://archive.ubuntu.com/ubuntu/pool/main/libn/libnftnl/libnftnl_1.1.5-1.dsc")
packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/n/nftables/nftables_0.9.3-2.debian.tar.xz")
packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/n/nftables/nftables_0.9.3.orig.tar.bz2.asc")
packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/n/nftables/nftables_0.9.3.orig.tar.bz2")
packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/n/nftables/nftables_0.9.3-2.dsc")

for package in "${packages[@]}"; do
    build "${package}"
done
